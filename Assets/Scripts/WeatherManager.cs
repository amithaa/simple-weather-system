﻿using UnityEngine;

public class WeatherManager : MonoBehaviour
{
    public ParticleSystem rainParticles;
    public Renderer rippleRenderer;

    public void OnWeatherChange(float _value)
    {
        switch((int)_value)
        {
            case 0: InitialiseSunnyWeather();
                break;

            case 1:
                InitialiseRainyWeather();
                break;
        }
    }

    private void InitialiseRainyWeather()
    {
        rainParticles.Play();

        //Set values of the vfx 
        rippleRenderer.material.SetFloat("_Smoothness", 1.4f);
        rippleRenderer.material.SetFloat("_RippleStrength", 0.25f);
        rippleRenderer.material.SetFloat("_PuddleStrenth", 0.2f);

        RenderSettings.fog = true;
    }

    private void InitialiseSunnyWeather()
    {
        rainParticles.Stop();

        //Set values of the vfx 
        rippleRenderer.material.SetFloat("_Smoothness", 0.3f);
        rippleRenderer.material.SetFloat("_RippleStrength", 0.0f);
        rippleRenderer.material.SetFloat("_PuddleStrenth", 0.0f);

        RenderSettings.fog = false;
    }
}
